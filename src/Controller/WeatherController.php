<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\WeatherRepository;

class WeatherController extends AbstractController
{
    public function cityAction(
        $city, 
        CityRepository $cityRepository,
        CountryRepository $countryRepository,
        WeatherRepository $weatherRepository
    ): Response
    {
        $city = $cityRepository->findOneByCityName($city);
        if ($city == null) {
            throw new \Exception('Something went wrong!');
        }


        $country = $city->getCountryId();
        $weather = $weatherRepository->findOneByCityId($city->getId());
        if ($weather == null) {
            throw new \Exception('Something went wrong!');
        }

        return $this->render('weather/city.html.twig', [
            'country' => $country,
            'city' => $city,
            'weather' => $weather,
        ]);        


        // $city = $cityRepository->findOneByCityId($cityId);
        // if ($city == null) {
        //     throw new \Exception('Something went wrong!');
        // }

        // $country = $city->getCountryId();

        // $weather = $weatherRepository->findOneByCityId($cityId);
        // if ($weather == null) {
        //     throw new \Exception('Something went wrong!');
        // }

        // return $this->render('weather/city.html.twig', [
        //     'country' => $country,
        //     'city' => $city,
        //     'weather' => $weather,
        // ]);
    }
}
